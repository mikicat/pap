#include <pthread.h>
#include <semaphore.h>

#define LIFOSIZE 3
#define LOOP 50
typedef struct {
        int elem[LIFOSIZE];
        int index;
        pthread_mutex_t mutex; // mutext to protect access to LIFO
        sem_t write_sem;       // when 0, buffer is full. Initialized
                               // to BUFSIZE i.e. BUFSIZE number of producer
                               // threads can  write to it before full
        sem_t read_sem;        // when 0, buffer is empty. Initialized
                               // to 0, so no consumer can read from buffer
                               // until a producer thread posts to read_sem
} lifo_t;
lifo_t lifo;

void lifoInit(lifo_t *b);
void lifoDestroy(lifo_t *b);
void lifoAdd(lifo_t *b, int value);
int  lifoDel(lifo_t *b);

void *producer (void *args) {
    for (int i = 0; i < LOOP; i++) {
            // to complete, directly accessing global variable buffer
            // inserting value i in LIFO buffer in each iteration of the loop
        sem_wait(&lifo->write_sem);
        pthread_mutex_lock(&lifo->mutex);
        lifoAdd(&lifo,i);
	pthread_mutex_unlock(&lifo->mutex);
	sem_post(&lifo->read_sem);

    }
    pthread_exit(0);
}
void *consumer (void *args) {
    for (int i = 0; i < LOOP; i++) {
            // to complete, directly accessing global variable buffer
            // printing one value extracted from LIFO buffer in each iteration of the loop
        sem_wait(&lifo->read_sem);
        pthread_mutex_lock(&lifo->mutex);
        lifoDel(&lifo);
	pthread_mutex_unlock(&lifo->mutex);
	sem_post(&lifo->write_sem);

    }
    pthread_exit(0);
}

int main() {
    pthread_t producer_thread, consumer_thread;

    printf("Pthreads implementation for a LIFO buffer, one producer/one consumer, buffer size %d\n", LIFOSIZE);

    bufferInit(&lifo);

    pthread_create(&producer_thread, NULL, producer, NULL);
    pthread_create(&consumer_thread, NULL, consumer, NULL);

    pthread_join(producer_thread, NULL);
    pthread_join(consumer_thread, NULL);

    bufferDestroy(&lifo);
    return 0;
}

void lifoInit(lifo_t *b) {
    b->index = 0;
    sem_init(b->write_sem, NULL, LIFOSIZE);
    sem_init(b->read_sem, NULL, 0);
    pthread_mutex_init(&b->mutex);
}
void lifoDestroy(lifo_t *b) {
    sem_destroy(&lifo->write_sem);
    sem_destroy(&lifo->read_sem);
    b->index = 0
    pthread_mutex_destroy (b->mutex);
}

void lifoAdd(lifo_t *b, int value) {
    b->elem[b->index++] = value;
}

int lifoDel(lifo_t *b) {
    return b->elem[--b->index];
}
