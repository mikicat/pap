#include "libminiomp.h"

// Library constructor and desctructor
void init_miniomp(void) __attribute__((constructor));
void fini_miniomp(void) __attribute__((destructor));

void
init_miniomp(void) {
  //printf ("mini-omp is being initialized\n");
  // Parse OMP_NUM_THREADS environment variable to initialize nthreads_var internal control variable
  parse_env();
  // Initialize pthread and parallel data structures 

  // Initialize Pthread thread-specific data, now just used to store the OpenMP thread identifier
  pthread_key_create(&miniomp_specifickey, NULL);
  pthread_setspecific(miniomp_specifickey, (void *) 0); // implicit initial pthread with id=0
  // Initialize OpenMP default locks and default barrier
  // Initialize OpenMP workdescriptors for single 
  // Initialize OpenMP task queue for task and taskloop
  pthread_mutex_init(&miniomp_default_lock, NULL);
  pthread_mutex_init(&aux, NULL);
  pthread_mutex_init(&miniomp_single.lock, NULL);
  // Init count de single
  miniomp_single.count = (unsigned int *) malloc(sizeof(int) * MAX_THREADS);
  for (int i=0; i<MAX_THREADS; i++) miniomp_single.count[i]=0;
  // Create key for index of single
  pthread_key_create(&miniomp_single.index, NULL);
  // Inicialitzem cua de tasques
  miniomp_taskqueue = TQinit(MAXELEMENTS_TQ);
}

void
fini_miniomp(void) {
  // delete Pthread thread-specific data
  pthread_key_delete(miniomp_specifickey);

  // free other data structures allocated during library initialization
  pthread_mutex_destroy(&miniomp_default_lock);
  pthread_mutex_destroy(&aux);
  pthread_mutex_destroy(&miniomp_single.lock);
  pthread_barrier_destroy(&miniomp_barrier);
  pthread_key_delete(miniomp_single.index);

  TQdestroy(miniomp_taskqueue);
  //printf ("mini-omp is finalized\n");
}
