#include "libminiomp.h"

// Declaratiuon of global variable for single work descriptor
miniomp_single_t miniomp_single;
pthread_key_t single_id_key;

// This routine is called when first encountering a SINGLE construct. 
// Returns true if this is the thread that should execute the clause. 

bool
GOMP_single_start (void) {
  //printf("TBI: Entering into single, don't know if anyone else arrived before, I proceed\n");
  // Agafem id per actualitzar count
  int id = (int)(long) pthread_getspecific(miniomp_single.index);
  // Actualitzem count
  pthread_mutex_lock(&miniomp_single.lock);
  unsigned int val = miniomp_single.count[id]++;
  if (val == omp_get_num_threads()-1) miniomp_single.count[id] = 0;
  pthread_mutex_unlock(&miniomp_single.lock);
  // Actualitzem id (proxim single)
  pthread_setspecific(miniomp_single.index, (void *)(long)((++id)%omp_get_max_threads()));
  return(!val);
}
/*
 * Cal fer la struct miniomp_single_t
 * S'ha de: tenir en compte quants threads (diferents) han passat
 * pel single per poder reinicialitzar el single 
 *
 */
