// Type declaration for single work sharing descriptor
typedef struct {
    // complete the definition of single descriptor
    pthread_key_t index; // Clau index especifica
    unsigned int * count; // Vector de comptadors
    pthread_mutex_t lock;
} miniomp_single_t;

// Declaration of global variable for single work descriptor
extern miniomp_single_t miniomp_single;

// Functions implemented in this module
bool GOMP_single_start (void);
