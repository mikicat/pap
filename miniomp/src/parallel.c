#include "libminiomp.h"

// This file implements the PARALLEL construct as part of your 
// miniomp runtime library. parallel.h contains definitions of 
// data types used here

// Declaration of array for storing pthread identifier from 
// pthread_create function
pthread_t *miniomp_threads;

// Global variable for parallel descriptor
miniomp_parallel_t *miniomp_parallel;

// Declaration of per-thread specific key
pthread_key_t miniomp_specifickey;

// This is the prototype for the Pthreads starting function
void *
worker(void *args) {
  // insert all necessary code here for:
  //   1) save thread-specific data
  //   2) invoke the per-threads instance of function encapsulating the parallel region
  //   3) exit the function
  miniomp_parallel_t * descriptor = (miniomp_parallel_t *) (args);  
  pthread_setspecific(miniomp_specifickey, (void *)(long)(descriptor->id));
  // Posar valor a index single (Inicialitzem a 0)
  pthread_setspecific(miniomp_single.index, (void *) 0);

  descriptor->fn(descriptor->fn_data);
  pthread_exit(NULL);
}

void
GOMP_parallel (void (*fn) (void *), void *data, unsigned num_threads, unsigned int flags) {
  if(!num_threads) num_threads = omp_get_num_threads();
//  printf("Starting a parallel region using %d threads\n", num_threads);
  miniomp_threads = (pthread_t *) malloc(sizeof(pthread_t) * num_threads);
  miniomp_parallel = (miniomp_parallel_t *) malloc(sizeof(miniomp_parallel_t) * num_threads);
  // Init barreres
  pthread_barrier_init(&miniomp_barrier, NULL, num_threads);

  for (int i=1; i<num_threads; i++) {
     miniomp_parallel[i].id = i;
     miniomp_parallel[i].fn_data = data;
     miniomp_parallel[i].fn = fn;
     pthread_create (&miniomp_threads[i], NULL, worker, &miniomp_parallel[i]);
  }
  fn(data);
  for (int i=1; i<num_threads; ++i) {
     pthread_join(miniomp_threads[i], NULL);
  }
}

